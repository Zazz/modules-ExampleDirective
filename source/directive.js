var module = angular.module('zazz.example-directive', []);

module.directive('zazzExampleDirective', function () {
    return {
        restrict: 'E',
        template: '<h3>This is my example directive</h3>'
    };
});